from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174034.models import User
# Create your views here.
def index(request):
    return render(request, 'ujian_aplikasi_1174034/index_1174034.html')

def user_1174034(request):
    user = User.objects.all()
    userdict = {'users': user}
    return render(request,'ujian_aplikasi_1174034/users_1174034.html',context= userdict)
    