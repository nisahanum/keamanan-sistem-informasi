from django.shortcuts import render
from ujian_aplikasi_1174040.models import User

def index(request):
    return render(request,'ujian_aplikasi_1174040/index_1174040.html')

def users(request):
    model = User.objects.all()
    data = {'daftar': model}
    return render(request,'ujian_aplikasi_1174040/users_1174040.html', data)

# Create your views here.
