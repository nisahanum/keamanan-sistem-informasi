import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174040.settings')

import django
django.setup()

import random
from ujian_aplikasi_1174040.models import User
from faker import Faker

fakegen = Faker()
def populate(N):
    for entry in range (N):
        fake_first = fakegen.first_name_male()
        fake_last =  fakegen.last_name()
        fake_email = fakegen.email()

        usr = User.objects.get_or_create(first_name=fake_first, last_name=fake_last, email=fake_email)

if __name__=='__main__':
    print("Populating the database.....Please Wait")
    populate(30)
    print("Populating Complete")