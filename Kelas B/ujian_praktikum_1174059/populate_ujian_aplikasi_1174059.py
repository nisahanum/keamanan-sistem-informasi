import os
import django
import random
from ujian_aplikasi_1174059.models import first_name, last_name, email
from faker import Faker

os.environ.setdefault('DJANGO_SETTINGS_MODEUL',
                      'ujian_praktikum_1174059.settings')
django.setup()

fakegen = Faker()
topics = ['Search', 'Social', 'News', 'Games']


def add_topic():
    t = first_name.objects.get_or_create(top_name=random.choice(topics))[0]
    t.save()
    return t


def populate(N=5):
    for entry in range(N):
        top = add_topic()

        fake_name = fakegen.company()

        lm = last_name.objects.get_or_create(
            topic=top, name=fake_name)[0]

        acc_rec = email.objects.get_or_create(
            name=lm)[0]


if __name__ == '__main__':
    print("Populating the database........ Please Wait")
    populate(2)
    print("Populating Complete")
