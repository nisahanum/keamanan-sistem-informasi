from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174047.models import User
# Create your views here.

def index(request):
    return render(request,'ujian_aplikasi_1174047/index_1174047.html')

def User_1174047(request):
    result = User.objects.all()
    datasender = {"daftaruser": result}
    return render(request,'ujian_aplikasi_1174047/users_1174047.html', datasender)