import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174050.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174050.models import User
from faker import Faker

fakegen = Faker()
users = ['Dika','Diki','Diko','Sukadi','Maryono']

def populate(N=5):
    for entry in range(N):
        fake_lastname = fakegen.last_name()
        fake_email = fakegen.email()

        user= User.objects.get_or_create(first_name=random.choice(users),last_name=fake_lastname,email=fake_email)[0]

if __name__=='__main__':
    print("Populating the database.....Please Wait!")
    populate(30)
    print("Populating Complete!")