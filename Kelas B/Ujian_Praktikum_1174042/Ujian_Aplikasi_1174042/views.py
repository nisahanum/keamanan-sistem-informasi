from django.shortcuts import render
from django.http import HttpResponse
from Ujian_Aplikasi_1174042.models import User
# Create your views here.
def index(request):
    return render(request, 'Ujian_Aplikasi_1174042/index_1174042.html')

def user_1174042(request):
    user = User.objects.all()
    userdict = {'users': user}
    return render(request,'ujian_aplikasi_1174042/users_1174042.html',context= userdict)
    