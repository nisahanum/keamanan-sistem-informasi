from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email = models.EmailField(max_length=50,unique=True)
    
    class Meta:
        db_table = "user"

    def __str__(self):
        return self.email