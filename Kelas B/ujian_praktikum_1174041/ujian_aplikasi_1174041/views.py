from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174041.models import User
# Create your views here.

def index(request):
    return render(request,'ujian_aplikasi_1174041/index_1174041.html')

def User_1174041(request):
    result = User.objects.all()
    datasender = {"daftaruser": result}
    return render(request,'ujian_aplikasi_1174041/users_1174041.html', datasender)