from django.shortcuts import render

from .models import Users
# Create your views here.

def index(request):
    return render(request, 'ujian_aplikasi_1174084/index_1174084.html')

def user_detail(request):
    user = Users.objects.all()
    context = {
        'User':user,
    }
    return render(request, 'ujian_aplikasi_1174084/users_1174084.html', context)
