from django.shortcuts import render
from django.http import HttpResponse
from .models import User

# Create your views here.

def index(request):
    return render (request,'ujian_aplikasi_1174074/index_1174074.html')

def users(request):
    user_profiles = User.objects.all()
    context= {'allvideos': user_profiles}
    return render (request,'ujian_aplikasi_1174074/users_1174074.html',context)