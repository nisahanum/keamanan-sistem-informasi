import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174074.settings')

import django
django.setup()

import random
from ujian_aplikasi_1174074.models import User
from faker import Faker

fakegen = Faker()

def PO_1174074(N):
    for entry in range(N):

        fake_firstName = fakegen.first_name()
        fake_lastName = fakegen.last_name_male()
        fake_email = fakegen.email()

        user_profiles = User.objects.get_or_create(first_name=fake_firstName,last_name=fake_lastName,email=fake_email)[0]
        

if __name__=='__main__':
    print("Populating database...... bersabar!")
    PO_1174074(30)
    print("Populating complete")