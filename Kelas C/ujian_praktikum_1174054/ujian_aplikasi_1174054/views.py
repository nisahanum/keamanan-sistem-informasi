from django.shortcuts import render
from .models import User


# Create your views here.
def index(request):
    return render(request, 'ujian_aplikasi_1174054/index_1174054.html')

def userViews(request):
    pengguna = User.objects.all()
    return render(request, 'ujian_aplikasi_1174054/users_1174054.html', {'pengguna': pengguna})