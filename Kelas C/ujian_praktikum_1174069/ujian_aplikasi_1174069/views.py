from django.shortcuts import render
from .models import user

# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174069/index_1174069.html')

def userViews(request):
    pengguna = user.objects.all()
    return render(request, 'ujian_aplikasi_1174069/users_1174069.html', {'pengguna': pengguna})