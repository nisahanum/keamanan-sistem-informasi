from django.shortcuts import render
from django.http import HttpResponse
from .models import Users
# Create your views here.
def index (request) :
	return render(request,'index_1174071.html')

def users(request):
    users_data = Users.objects.all()
    context= {'allvideos': users_data}
    return render (request,'users_1174071.html',context)
