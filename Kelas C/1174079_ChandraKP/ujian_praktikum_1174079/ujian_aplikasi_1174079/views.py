from django.shortcuts import render
from django.http import HttpResponse
from .models import User

# Create your views here.

def index(request):
    return render (request,'ujian_aplikasi_1174079/index_1174079.html')

def users(request):
    user_profiles = User.objects.all()
    context= {'allvideos': user_profiles}
    return render (request,'ujian_aplikasi_1174079/users_1174079.html',context)