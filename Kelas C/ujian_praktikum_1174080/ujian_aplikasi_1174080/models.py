from django.db import models

# Create your models here.

class User(models.Model):
    firstname = models.CharField(max_length=225)
    lastname = models.CharField(max_length=225)
    useremail = models.EmailField(max_length=225, unique=True)

    def __str__(self):
        return self.firstname