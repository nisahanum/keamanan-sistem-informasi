import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_praktikum_1174080.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174080.models import User
from faker import Faker

fakegen = Faker()

def PO_1174080 (N):
    for entry in range(0,N):
        fake_firstName = fakegen.first_name()
        fake_lastName = fakegen.last_name()
        fakeemail = fakegen.email()

        user_profiles_= User.objects.get_or_create(firstname=fake_firstName,lastname=fake_lastName,useremail=fakeemail)[0]

if __name__=='__main__':
    print("populating database....HandiHermawan!")
    PO_1174080(30)
    print("Data Berhasil Diinput")