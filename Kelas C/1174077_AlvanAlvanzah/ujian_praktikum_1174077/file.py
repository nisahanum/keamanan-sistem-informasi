import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_praktikum_1174077.settings')

import django

django.setup()

import random
from ujian_aplikasi_1174077.models import Users
from faker import Faker

fakegen = Faker()

def po_1174077(N=30):
    for entry in range (N):
        fake_first = fakegen.first_name()
        fake_last = fakegen.last_name_male()
        fake_email = fakegen.email()

        Users.objects.get_or_create(first_name=fake_first, last_name=fake_last, email=fake_email)[0]

if __name__=='__main__':
    print("Populating the database ....Plase Wait!")
    po_1174077(30)
    print("Populate Complate!")