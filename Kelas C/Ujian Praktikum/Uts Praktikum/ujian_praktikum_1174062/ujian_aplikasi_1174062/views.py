from django.shortcuts import render
from .models import User

# Create your views here.
def index(request):
    return render(request, 'ujian_aplikasi_1174062/index_1174062.html')

def userViews(request):
    pengguna = User.objects.all()
    return render(request, 'ujian_aplikasi_1174062/users_1174062.html', {'pengguna': pengguna})