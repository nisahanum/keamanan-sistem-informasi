from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174004.models import User
# Create your views here.
def index(request):
	return render(request,'ujian_aplikasi_1174004/index_1174004.html')

def user_view(request):
	return render(request,'ujian_aplikasi_1174004/user_1174004.html')
