import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174006.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174006.models import User
from faker import Faker

fakegen = Faker()
users = ['DwiS','DwiY','Damara','Evie','Sri']

def populate(N=30):
	for entry in range(N):
		fake_lastname = fakegen.last_name()
		fake_email = fakegen.email()

		user= User.objects.get_or_create(first_name=random.choice(users),last_name=fake_lastname,email=fake_email)[0]

if __name__=='__main__':
    print("Populating the database.....Please Wait!")
    populate()
    print("Populating Complete!")