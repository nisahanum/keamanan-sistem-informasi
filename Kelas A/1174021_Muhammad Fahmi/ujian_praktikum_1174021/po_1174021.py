import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_praktikum_1174021.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174021.models import User
from faker import Faker

fakegen = Faker()
namadepan = ['evie','dwisep','dwiyul','damara','sri']

def populate(N=30):
    for entry in range(0,N):
        fakelast = fakegen.last_name()
        fakemail = fakegen.email()

        namaakhir = User.objects.get_or_create(firstname=random.choice(namadepan),lastname=fakelast,useremail=fakemail)[0]

if __name__=='__main__':
    populate(30)
    print("Data Berhasil Diinput")