from django.db import models

# Create your models here.


class User(models.Model):
    usermail = models.EmailField(max_length=255, unique=True)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.usermail)
