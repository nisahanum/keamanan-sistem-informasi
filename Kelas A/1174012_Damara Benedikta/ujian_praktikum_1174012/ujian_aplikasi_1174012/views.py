from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174012.models import User
# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174012/index_1174012.html')

def user_1174012(request):
    user = User.objects.all()
    userdict = {'users': user}
    return render(request,'ujian_aplikasi_1174012/user_1174012.html',context= userdict)
