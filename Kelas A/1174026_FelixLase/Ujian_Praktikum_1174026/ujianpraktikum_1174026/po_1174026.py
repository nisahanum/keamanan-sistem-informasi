import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujianpraktikum_1174026.settings')

import django

django.setup()

import random
from ujian_aplikasi_1174026.models import User
from faker import Faker

fakegen = Faker()
depan = ['Dwi.S','Dwi.Y','Evi','Damara','Sri']

def populate(N=5):
    
    for entry in range (N):
        lname = fakegen.last_name()
        email = fakegen.email()

        satu = User.objects.get_or_create(firstname = random.choice(depan), lastname = lname, email = email)

if __name__=='__main__':
    print("Populating the database..... Please Wait!")
    populate(90)
    print("Populating Compl")