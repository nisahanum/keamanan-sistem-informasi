from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174026.models import User

# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174026/index_1174026.html')

def user(request):
    satu = User.objects.all()
    dua = {
        'hasil': satu
    }
    return render(request,'ujian_aplikasi_1174026/user_1174026.html',context=dua)