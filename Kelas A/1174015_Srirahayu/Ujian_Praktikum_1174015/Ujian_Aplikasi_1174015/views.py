from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174015.models import User
# Create your views here.


def index(request):
    return render(request, 'ujian_aplikasi_1174015/index_1174015.html')


def users(request):
    sdata = User.objects.all()
    data = {
        'hasil': sdata,
    }
    return render(request, 'ujian_aplikasi_1174015/users_1174015.html', context=data)
