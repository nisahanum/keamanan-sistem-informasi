from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174002.models import User
# Create your views here.

def index(request):
    return render(request,'ujian_aplikasi_1174002/index_1174002.html')

def users(request):
    sdata = User.objects.all()
    data = {
        'hasil': sdata,
    }
    return render(request, 'ujian_aplikasi_1174002/users_1174002.html', context=data)