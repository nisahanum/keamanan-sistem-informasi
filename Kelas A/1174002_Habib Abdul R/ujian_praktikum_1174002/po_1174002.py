import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174002.settings')

import django
django.setup()

import random 
from ujian_aplikasi_1174002.models import User
from faker import Faker

fakegen = Faker()
users = ['Viska','Lisna','Lola','Tasya','Hesty']

def populate(N=30):
    for entry in range(N):
        firstname = fakegen.first_name_female()
        last = fakegen.last_name()
        mail = fakegen.email()

        pengguna = User.objects.get_or_create(firstname=random.choice(users),lastname=last,email=mail)


if __name__=='__main__':
    print("Populating the database..... Please,wait")
    populate(30)
    print("Populating Complete")