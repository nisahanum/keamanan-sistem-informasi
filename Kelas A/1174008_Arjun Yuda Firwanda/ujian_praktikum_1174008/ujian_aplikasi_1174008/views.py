from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174008.models import User

# Create your views here.

def index(request):
	return render(request,'ujian_aplikasi_1174008/index_1174008.html')

def user_view(request):
	user = User.objects.all()
	userdict = {'users': user}
	return render(request,'ujian_aplikasi_1174008/user_1174008.html',context=userdict)
