from django.db import models

# Create your models here.

class User(models.Model):
	useremail = models.EmailField(max_length=225, unique=True)
	firstname = models.CharField(max_length=225)
	lasttname = models.CharField(max_length=225)

	def __str__(self):
		return "{}".format(self.useremail)